const inquirer = require('inquirer')
const fs = require('fs')
const shell = require('shelljs')

shell.config.silent = true

inquirer
    .prompt([
        {
            type: 'text',
            name: 'site',
            message: 'Please enter the site you would like to scrap (https://www.demosite.com)'
        },
    ])
    .then(resp => {
        const { site } = resp
        
        // Getting contents of the website
        let { stdout: contents } = shell.exec(`curl ${site}`)
        
        // Getting contents within the <body></body> tags
        let body = contents.substring(contents.indexOf('<body>'), contents.indexOf('</body>') + 7)
        
        // Splitting the contents into an array
        let words = body.split(' ')

        let indices = []
        let notOk = true
        let i = 0

        while (notOk) {
            let start = words.indexOf('<style>') || words.indexOf('<script>')
            if (start >= 0) {
                let end = words.indexOf('</style>') || words.indexOf('</script>')
                if (end >= 0) {
                    indices = [start, end]
                    words = words.filter((_, i) => i < start && i > end)
                    continue
                }
            }

            i++ 
            notOk = false
        }

        // Removing any tags and special characters
        words = words.filter(word => !word.includes('function') && !word.includes('<') && !word.includes('/') && !word.includes('style') && !word.includes('script') && !word.includes('class=') && !word.includes('>') && !(word.includes('(') && word.includes(')')) && !word.includes('\\'))
        
        let occurrence = {}

        for (let word of words) {
            if (!occurrence[word?.toLowerCase()])
                occurrence[word?.toLowerCase()] = words.filter(wrd => wrd.toLowerCase() === word.toLowerCase()).length
        }

        occurrence = JSON.stringify(occurrence)

        fs.writeFileSync('output.json', occurrence)
    })

// https://www.google.com