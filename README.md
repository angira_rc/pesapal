## Pesapal Demo
### Problem 1
For this problem I chose to use Node Js + Express as my main language and NGINX as my web server / reverse proxy.

The NGINX site configurations are found in the **nginx-ssl-config** file complete with SSL configurations.

I created the configurations with the assumption that the site will be hosted on **pesapaldemo.com** and that the SSL certificates are stored in the directory **/etc/letsencrypt/live**.

You run the application by navigating to the directory where **Problem 1** is located, running **npm install** to install all the application dependencies and then running **node app.js** to start the application.

You then start the browser and go to **http://localhost:4000**

### Problem 3
For this problem I chose to use Node Js and inquirer.js as my CLI interface engine.

You run the application by navigating to the directory where **Problem 3** is located, running **npm install** to install all the application dependencies and then running **node app.js** to start the application.

In the CLI, you will enter the site you would like to scrap in the format ***https://demosite.com***.

The application then will get the contents of the webpage and perform scrapping.