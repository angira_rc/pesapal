require('dotenv').config()

const path = require('path')
const express = require('express')

const { PORT } = process.env

const app = express()

app.use(express.static('public'))

app.get('/', (req, res) => res.sendFile(path.resolve(__dirname, 'public', 'index.html')))

app.listen(PORT, err => {
    if (err) 
        throw err
        
    console.log(`\nRunning ...\n`)
})
